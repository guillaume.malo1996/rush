require 'CSV'

class Player
    def self.attr_accessor(*vars)
        @attributes ||= []
        @attributes.concat vars
        super(*vars)
    end

    def self.attributes
        @attributes
    end

    def attributes
        self.class.attributes
    end

    def self.sortables
        [
            "name", #first line is default sort order
            "yds",
            "lng",
            "td"
        ]
    end

    def self.to_csv(players)
        CSV.generate(headers: true) do |csv|
          csv << Player.attributes
    
          players.each do |user|
            csv << attributes.map{ |attr| user.send(attr) }
          end
        end
    end

    attr_accessor :name,
        :team,
        :pos,
        :att,
        :att_per_g,
        :yds,
        :avg,
        :yds_per_g,
        :td,
        :lng,
        :first,
        :first_percent,
        :twenty_plus,
        :fourty_plus,
        :fum

    def initialize(name, team, pos, att, att_per_g, yds, avg, yds_per_g,
                    td, lng, first, first_percent, twenty_plus, fourty_plus, fum)
        @name = name
        @team = team
        @pos = pos
        @att = att
        @att_per_g = att_per_g
        if yds.class == String
            @yds = yds.gsub(",", "").to_i
        else
            @yds = yds
        end
        @avg = avg
        @yds_per_g = yds_per_g
        @td = td
        @lng = lng.to_s
        @first = first
        @first_percent = first_percent
        @twenty_plus = twenty_plus
        @fourty_plus = fourty_plus
        @fum = fum
    end

    def self.all
        data = JSON.parse(File.read(Rails.root.join('data', 'players.json')))
        data.map do |hash|
            Player.new(
                hash["Player"],
                hash["Team"],
                hash["Pos"],
                hash["Att"],
                hash["Att/G"],
                hash["Yds"],
                hash["Avg"],
                hash["Yds/G"],
                hash["TD"],
                hash["Lng"],
                hash["1st"],
                hash["1st%"],
                hash["20+"],
                hash["40+"],
                hash["FUM"]
            )
        end
    end
end