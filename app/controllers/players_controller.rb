class PlayersController < ApplicationController
    def index
        page_size = 100
        @players = Player.all

        @player_name_filter = params["player_name"] && params["player_name"].downcase
        @players.filter! {|p| p.name.downcase.include?(@player_name_filter)} if @player_name_filter
        @max_page = (@players.length / page_size.to_f).ceil
        @page = sanitized_page(params["page"], @max_page)

        @sort_type = sanitized_sort_type(params["sorted_by"])
        @sort_direction = sanitized_sort_direction(params["sort_direction"])

        if @sort_type == "lng"
            @players.sort! do |p1, p2| 
                a = p1.send(@sort_type)
                b = p2.send(@sort_type)

                # sort touchdowns before non touchdowns for equal values
                a = ((a.class == String && a.end_with?("T")) ? 0.5 : 0) + a.to_i
                b = ((b.class == String && b.end_with?("T")) ? 0.5 : 0) + b.to_i
                a <=> b
            end
        else
            @players.sort! {|p1, p2| p1.send(@sort_type) <=> p2.send(@sort_type)}
        end
        @players.reverse! if @sort_direction == "DESC"

        @players = @players.slice((@page - 1) * page_size, page_size)
        respond_to do |format|
            format.html
            format.csv { send_data Player.to_csv(@players), filename: "players.csv" }
        end
    end

    def export_csv
        send_data Player.to_csv(@players), filename: "players.csv"
    end

private
    def sanitized_page(p, max_page)
        return 1 unless p

        p = p.to_i
        return 1 if p == 0
        return max_page if p > max_page
        return p
    end

    def sanitized_sort_type(sort_type)
        sortables = Player.sortables
        return sortables[0] unless sort_type
        return sortables[0] unless sortables.include?(sort_type)
        return sort_type
    end

    def sanitized_sort_direction(sort_direction)
        return "ASC" unless sort_direction
        return "ASC" unless ["ASC", "DESC"].include?(sort_direction)
        return sort_direction
    end
end
